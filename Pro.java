package zuoye;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.Properties;

//4 ：列出java.util.Properties中所有的属性和方法
public class Pro {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Class a = Properties.class;
		Field b[] = a.getDeclaredFields();
		System.out.println("所有的属性");
		for (Field c : b) {
			System.out.println(c);
		}
		Method d[] = a.getDeclaredMethods();
		System.out.println("所有的方法");
		for (Method c : d) {
			System.out.println(c);
		}
		Constructor e[] = a.getDeclaredConstructors();
		System.out.println("所有构造方法");
		for (Constructor c : e) {
			System.out.println(c);
		}
	}

}
