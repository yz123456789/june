package zuoye;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

//1:福彩“双色球”是民政部公开发行的彩票，每注投注号码由6个红色球号码和1个蓝色球号码组成。红色球号码从1--33中选择；蓝色球号码从1--16
//中选择。购买时，红色球号码中选择6个号码，从蓝色球号码中选择1个号码，组合成1注投注号码.
//要求生成一注中奖号码，务必按照红球6个号码在前，1个篮球号码在后的方式排列，同时红球号按照顺序排列。并序列化到文件中永久保存
//（提示：可以用List保存中奖号码）
public class CaiPiao {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		List a = new ArrayList();
		int b;
		Random c = new Random();
		while (a.size() < 6) {
			b = c.nextInt(33);
			if (!a.contains(b)) {
				a.add(b);
			}
		}
		Collections.sort(a);
		a.add(c.nextInt(16));
		System.out.println(a);
	}

}
